package model.matrices;

import model.interfaces.AbstractMatrix;

import java.util.LinkedHashMap;

public class LinkedHashSparseMatrix extends AbstractMatrix {
    public LinkedHashSparseMatrix(int n, int m) {
        this.n = n;
        this.m = m;
        matrix = new LinkedHashMap<>();
    }
}
