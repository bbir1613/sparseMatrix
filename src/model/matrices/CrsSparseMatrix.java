package model.matrices;

import model.interfaces.Key;
import model.interfaces.SparseMatrix;

import java.io.IOException;
import java.util.Map;

public class CrsSparseMatrix implements SparseMatrix<Key> {

    @Override
    public void add(int i, int j, int value) {

    }

    @Override
    public int get(int i, int j) {
        return 0;
    }

    @Override
    public int getByKey(Key k) {
        return 0;
    }

    @Override
    public void putByKey(Key k, int value) {

    }

    @Override
    public SparseMatrix multiply(SparseMatrix b) {
        return null;
    }

    @Override
    public SparseMatrix asyncMultiply(SparseMatrix b) {
        return null;
    }

    @Override
    public void writeToFile(String fileName) throws IOException {

    }

    @Override
    public int getN() {
        return 0;
    }

    @Override
    public int getM() {
        return 0;
    }

    @Override
    public Map<Key, Integer> getMatrix() {
        return null;
    }
}
