package model.matrices;

import model.interfaces.AbstractMatrix;

import java.util.HashMap;

public class HashSparseMatrix extends AbstractMatrix {

    public HashSparseMatrix(int n, int m) {
        this.n = n;
        this.m = m;
        matrix = new HashMap<>();
    }
}
