package model.interfaces;

import model.matrices.HashSparseMatrix;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public abstract class AbstractMatrix implements SparseMatrix<Key> {
    protected int n;
    protected int m;
    protected Map<Key, Integer> matrix;

    @Override
    public void add(int i, int j, int value) {
        matrix.put(new Key(i, j), value);
    }

    @Override
    public int get(int i, int j) {
        return matrix.getOrDefault(new Key(i, j), 0);
    }

    private void validMatrices(SparseMatrix b) {
        if (matrix == null || b == null || n != b.getN()) throw new RuntimeException("Invalid matrix multiplication");
    }

    @Override
    public SparseMatrix multiply(SparseMatrix ot) {
        validMatrices(ot);
        SparseMatrix<Key> r = new HashSparseMatrix(n, ot.getM());
        Map<Key, Integer> b = ((Map<Key, Integer>) ot.getMatrix());
        matrix.entrySet().stream().forEach(keyA -> b.entrySet().stream().forEach(keyB -> {
            if (keyA.getKey().j == keyB.getKey().i) {
                Key key = new Key(keyA.getKey().i, keyB.getKey().j);
                int elem = r.getByKey(key) + keyA.getValue() * keyB.getValue();
                if (elem != 0) {
                    r.putByKey(key, elem);
                } else {
                    r.getMatrix().remove(key);
                }
            }
        }));
        return r;
    }

    @Override
    public SparseMatrix asyncMultiply(SparseMatrix ot) {
        validMatrices(ot);
        SparseMatrix<Key> r = new HashSparseMatrix(n, ot.getM());
        Map<Key, Integer> b = ((Map<Key, Integer>) ot.getMatrix());
        matrix.entrySet().parallelStream().forEach(keyA -> b.entrySet().parallelStream().forEach(keyB -> {
            if (keyA.getKey().j == keyB.getKey().i) {
                Key key = new Key(keyA.getKey().i, keyB.getKey().j);
                synchronized (keyA) {
                    int elem = r.getByKey(key) + keyA.getValue() * keyB.getValue();
                    if (elem != 0) {
                        r.putByKey(key, elem);
                    } else {
                        r.getMatrix().remove(key);
                    }
                }
            }
        }));
        return r;
    }

    @Override
    public void writeToFile(String fileName) throws IOException {
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            out.println(String.format("%d,%d", n, m));
            out.flush();
            matrix.entrySet().forEach(key -> {
                Key k = key.getKey();
                Integer value = key.getValue();
//                System.out.println(String.format("%s %s %s", k, value, get(k.i, k.j)));
                out.println(String.format("%d,%d,%d", k.i, k.j, value));
                out.flush();
            });
        } finally {
            if (fw != null) {
                fw.close();
            }
        }
    }

    @Override
    public int getN() {
        return n;
    }

    @Override
    public int getM() {
        return m;
    }

    @Override
    public int getByKey(Key k) {
        return matrix.getOrDefault(k, 0);
    }

    @Override
    public void putByKey(Key k, int value) {
        matrix.put(k, value);
    }

    @Override
    public Map<Key, Integer> getMatrix() {
        return matrix;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                sb.append(get(i, j)).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

}

/*
    public HashSparseMatrix asyncMultiply(HashSparseMatrix b) {
        if (n != b.getN()) throw new RuntimeException("Invalid matrix multiplication");
        HashSparseMatrix r = new HashSparseMatrix(n, b.getM());
        matrix.keySet().parallelStream().forEach(key1 -> b.matrix.keySet().parallelStream().forEach(key2 -> {
            if (key1.j == key2.j) {
                synchronized (key1) {
                    Key key = new Key(key1.i, key2.j);
                    int elem = r.getByKey(key) + matrix.get(key1) * b.getByKey(key2);
                    if (elem != 0) {
                        r.putByKey(key, elem);
                    } else {
                        r.matrix.remove(key);
                    }
                }
            }
        }));//O(matrix.keySet().size()*b.matrix.keySet().size())/p
        return r;
    }
}
 */