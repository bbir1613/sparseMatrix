package model.interfaces;


public class Key {
    protected int i, j;

    protected Key(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public Key() {

    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Key)) return false;
        Key other = (Key) obj;
        return i == other.i && j == other.j;
    }

    @Override
    public int hashCode() {
        return String.format("%s , %s", i, j).hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s , %s", i, j);
    }
}
