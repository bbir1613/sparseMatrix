package model.interfaces;

import java.io.IOException;
import java.util.Map;

public interface SparseMatrix<T> {
    void add(int i, int j, int value);

    int get(int i, int j);

    int getByKey(T k);

    void putByKey(T k, int value);

    SparseMatrix multiply(SparseMatrix b);

    SparseMatrix asyncMultiply(SparseMatrix b);

    void writeToFile(String fileName) throws IOException;

    int getN();

    int getM();

    Map<T, Integer> getMatrix();
}
