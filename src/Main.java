import model.interfaces.SparseMatrix;
import model.matrices.HashSparseMatrix;
import model.matrices.LinkedHashSparseMatrix;
import util.Util;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        hashMap();
//        linkedHashMap();
    }

    private static void hashMap() throws IOException {
        System.out.println("hash map");
        SparseMatrix a = new HashSparseMatrix(4, 4);
        SparseMatrix b = new HashSparseMatrix(4, 4);
//        Util.initMatrix(4, 4, a);
//        Util.initMatrix(4, 4, b);
        a.add(0, 3, 1);
        a.add(1, 1, 1);
        a.add(1, 2, 1);
        a.add(2, 1, 1);
        a.add(2, 2, 1);
        a.add(3, 0, 1);

        b.add(0, 2, 1);
        b.add(1, 1, 1);
        b.add(2, 1, 1);
        b.add(2, 3, 1);

//        Util.initMatrix(4, 4, a);
//        Util.initMatrix(4, 4, b);

        System.out.println(a);
        System.out.println(b);

        Util.Timer timer = new Util.Timer();
        timer.start();
//        SparseMatrix r = a.multiply(b);
        SparseMatrix r = a.asyncMultiply(b);
        timer.end();
        System.out.println(timer);
        System.out.print(r);
        r.writeToFile("a.txt");
    }


    private static void linkedHashMap() throws IOException {
        System.out.println("Linked hash map");
        SparseMatrix a = new LinkedHashSparseMatrix(2, 2);
        SparseMatrix b = new LinkedHashSparseMatrix(2, 2);
        Util.initMatrix(2, 2, a);
        Util.initMatrix(2, 2, b);
        System.out.println(a);
        System.out.println(b);

        Util.Timer timer = new Util.Timer();
        timer.start();
        SparseMatrix r = a.multiply(b);
        timer.end();
        System.out.println(timer);
        System.out.print(r);
        r.writeToFile("b.txt");
    }
/*
    private static void generateMatrices() {
        Thread th1 = new Thread(() -> {
            try {
                Util.generateMatix(100, 100, "a.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        th1.start();
        Thread th2 = new Thread(() -> {
            try {
                Util.generateMatix(100, 100, "b.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        th2.start();
        try {
            th1.join();
            th2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */
}
