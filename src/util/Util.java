package util;

import model.interfaces.SparseMatrix;

import java.util.concurrent.ThreadLocalRandom;

public class Util {

    private static final String SPLIT = ",";

    public static void initMatrix(int n, int m, SparseMatrix sp) {
        //Change with a file later
        for (int x = 0; x < n * m; ++x) {
            if (Math.random() > 0.5) {
                int i = ThreadLocalRandom.current().nextInt(0, n);
                int j = ThreadLocalRandom.current().nextInt(0, m);
                sp.add(i, j, 1);
            }
        }
    }

    public static class Timer {
        long start;
        long end;

        public void start() {
            start = System.nanoTime();
        }

        public void end() {
            end = System.nanoTime();
        }

        @Override
        public String toString() {
            return String.format("Elapsed time %f milliseconds", (end - start) * 0.000001);
        }
    }
}


//    public static void generateMatix(int n, int m, String fileName) throws IOException {
//        FileWriter fw = null;
//        try {
//            fw = new FileWriter(fileName);
//            BufferedWriter bw = new BufferedWriter(fw);
//            PrintWriter out = new PrintWriter(bw);
//            out.println(String.format("%d,%d", n, m));
//            out.flush();
//            for (int i = 0; i < n; ++i) {
//                for (int j = 0; j < m; ++j) {
//                    if (Math.random() > 0.5) {
//                        out.println(String.format("%d,%d,%d", i, j, 1));
//                        out.flush();
//                    }
//                }
//            }
//        } finally {
//            if (fw != null) {
//                fw.close();
//            }
//        }
//
//    }
